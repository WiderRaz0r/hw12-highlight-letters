let activeBtn
const keyBoard = {
    Enter: 'Enter',
    s: 's',
    e: 'e',
    o: 'o',
    n: 'n',
    l: 'l',
    z: 'z',
}

const buttons = document.querySelectorAll('.btn');


document.addEventListener('keydown', (e) => {
    if(keyBoard[`${e.key}`]) {
        buttons.forEach(function(item) {
            item.classList.remove('pressed-btn');
        });
        document.querySelector(`[data="${e.key}"]`).classList.add('pressed-btn');
        activeBtn = document.querySelector(`[data="${e.key}"]`);
    }
})

